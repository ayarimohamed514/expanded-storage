@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
package ellemes.expandedstorage.forge.datagen.providers;

import net.minecraft.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
