package ellemes.expandedstorage.api.client.gui;

public interface CopyFunction {
    void apply(int destX, int destY, int width, int height, int srcX, int srcY);
}
