package ellemes.expandedstorage.common.misc;

import net.minecraft.resources.ResourceLocation;

public interface ErrorlessTextureGetter {
    boolean expandedstorage$isTexturePresent(ResourceLocation location);
}
