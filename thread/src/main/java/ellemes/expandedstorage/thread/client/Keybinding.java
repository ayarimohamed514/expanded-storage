package ellemes.expandedstorage.thread.client;

public interface Keybinding {
    boolean matches(int keyCode, int scanCode);
}
