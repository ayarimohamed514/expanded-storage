# Best effort compatibility with Inventory tabs, notable issues:

- Opening chests from the wrong half doesn't update the active tab.
- Bottom row of inventories can overflow with tabs.
- No support for minecart chests (api not fully implemented)

Most of this package copies code from InventoryTabs which is licensed MIT.
You can therefore consider this package MIT even if our mod license changes.
